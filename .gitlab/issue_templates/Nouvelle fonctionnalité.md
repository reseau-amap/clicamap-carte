**Description**

*En quelques lignes, décrivez ici le contexte de la nouvelle fonctionnalité. Merci de préciser sur quel profil utilisateur cette fonctionnalité doit être déployée.

**Tests d'acceptation**

*Veuillez détailler ici les étapes permettant de tester si la nouvelle fonctionnalité est conforme aux attentes*

1. 
2.
3.
4.
5.

**MAQUETTE**

*Dans la mesure du possible, merci d'ajouter ici des maquettes ou captures d'écran permettant d'indiquer l'emplacement des différents boutons et les étapes réalisées sur les nouveaux écrans*