<?php
/*
Plugin Name: Affichage carte Clic'AMAP
Description: Affiche la carte des amaps et de fermes depuis ClicAMAP
Version: 3.1.0
License: GPL
Author: VitiCréation
Author: Sylvain Lehmann
Author URI: http://viticreation.fr
*/


require_once plugin_dir_path( __FILE__ ) . 'includes/ClicAmapCacheManager.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/ClicAmapAdminMenu.php';

class ClicamapCarte
{
    /**
     * Static property to hold our singleton instance
     *
     */
    static $instance = false;

    /**
     * @var ClicAmapCacheManager
     */
    private $cacheManager;

    /**
     * This is our constructor
     *
     * @return void
     */
    private function __construct() {
        $this->cacheManager = new ClicAmapCacheManager();

        $adminMenu = new ClicAmapAdminMenu();

        add_shortcode('map_clicamap', [$this, 'renderShortcode']);
        add_action('wp_enqueue_scripts', array( $this, 'plugin_libs'));
        add_action( 'wp_ajax_nopriv_clicamap_map_data', [$this, 'ajaxCarteData'] );
        add_action( 'wp_ajax_clicamap_map_data', [$this, 'ajaxCarteData'] );
        add_action( 'admin_menu', [$adminMenu, 'addAdminSubmenu'] );
        add_action( 'admin_init', [$adminMenu, 'initAdmin'] );
    }

    public function plugin_libs() {
        $pluginUrl = plugin_dir_url( __FILE__ );
        $cssUrl = $pluginUrl . 'assets/css' . DIRECTORY_SEPARATOR;
        $jsUrl = $pluginUrl . 'assets/js' . DIRECTORY_SEPARATOR;
        $imgUrl = $pluginUrl . 'assets/images' . DIRECTORY_SEPARATOR;

        wp_enqueue_style( 'leaflet'				, $cssUrl . 'leaflet.css'			);
        wp_enqueue_style( 'leaflet-ie'			, $cssUrl . 'leaflet.ie.css'		);
        wp_enqueue_style( 'amapk-front'			, $cssUrl . 'front.min.css'		);
        wp_enqueue_style( 'carte'				, $cssUrl . 'carte.css'	);

        wp_enqueue_script('leaflet'				, $jsUrl  . 'leaflet.js'		, array('jquery'	), null	, true	);
        wp_enqueue_script('leaflet-init' 		, $jsUrl  . 'init.js'		, [], null	, true	);
        wp_localize_script( 'leaflet-init', 'clicamap_map_obj', array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'nonce'    =>  wp_create_nonce( 'clicamap_carte_nonce' ),
            'imgUrl' => $imgUrl
        ) );
    }

    /**
     * If an instance exists, this returns it.  If not, it creates one and
     * retuns it.
     *
     * @return ClicamapCarte
     */
    public static function getInstance() {
        if ( !self::$instance )
            self::$instance = new self;
        return self::$instance;
    }


    public function renderShortcode()
    {
        $options = get_option('clicamap_options');
        ob_start();
        include('templates/carte.php');
        $var=ob_get_clean();

        return $var;
    }

    public function ajaxCarteData()
    {
        check_ajax_referer( 'clicamap_carte_nonce' );

        try {
            $data = $this->cacheManager->getData();
        } catch (\Exception $e) {
            wp_send_json_error();
        }

        wp_send_json($data);
    }
}

// Instantiate our class
ClicamapCarte::getInstance();
