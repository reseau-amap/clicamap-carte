# Description

Ce plugin Wordpress permet l'affichage d'une carte des fermes et des AMAP présentes sur Clic'AMAP.

# Installation

## Ajout de l'extension
L'installation se fait directement en envoyant le fichier clicamap-carte.zip vers le serveur.
Pour cela, une fois connecté à l'espace administrateur : 
- Aller dans extensions --> Ajouter -> `Téléverser une extension`
- Selectioner le fichier `clicamap-carte.zip`, puis cliquer sur `Installer`

## Configuration de l'extension

Avant de pouvoir être utilisée, l'extension doit être configurée. Cela se fait dans le menu `Réglages` -> `ClicAmap`.

Les options de configuration sont : 
- `Login` Identifiant de connexion à l'API.
- `Mot de passe` Mot de passe de connexion à l'API.
- `Durée du cache` Pour des raisons de performance, la liste des AMAP et des fermes récupérées depuis l'API est mise en cache. Ce champ détermine la durée du cache. Cela implique qu'une modification faite sur clicamap peut mettre au plus la durée de vie du cache pour etre répercutée sur la carte. La valeur conseillée est de 60 minutes.
- `Départements à inclure` Liste des codes de département à include, séparés par des virgules. Attention à bien mettre le 0 devant les départements à un seul chiffre. Par exemple `01` pour l'Ain.
- `Position initiale` Coordonnées GPS initiale de la carte lors de son chargement.
- `Zoom initial` Zoom initial de la carte lors de son chargement.

# Utilisation

L'affichage de la carte se fait par l'ajout du shortcode `[map_clicamap]` sur n'importe quelle page ou article.

# Licence

Copyright (c) 2019-2021 Réseau AMAP-AURA, publié sous licence AGPL.
