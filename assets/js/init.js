var vmAmapIcon = L.icon({
  iconUrl: clicamap_map_obj.imgUrl + 'carotte-rouge-50.png',
  iconSize: [20, 37],
  iconAnchor: [17, 36],
  popupAnchor: [-1, -32]
});
var vmPaysIcon = L.icon({
  iconUrl: clicamap_map_obj.imgUrl + 'pelle_vertf-50.png',
  iconSize: [30, 50],
  iconAnchor: [17, 36],
  popupAnchor: [-1, -32]
});
var map;
var amap;
var farm;

jQuery(document).ready(function ($) {
  var $map = $('#clicamap-map');
  map = L.map("clicamap-map", {
    "zoomstart": function () {
      var latlng = this.getCenter();
    }, "zoomend": function () {
      this.setView(latlng);
    }, "center": new L.LatLng($map.data('init-lat'), $map.data('init-long')), "zoom": $map.data('init-zoom'), "scrollWheelZoom": false
  });
  L.tileLayer("https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png", {
    "maxZoom": 18,
    "minZoom": 1,
    "detectRetina": true,
    "styleId": 999,
    "opacity": 1,
    "attribution": "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a>, Tiles courtesy of <a href=\"https://hot.openstreetmap.org/\" target=\"_blank\">Humanitarian OpenStreetMap Team</a>"
  }).addTo(map);

  amap = L.layerGroup();
  amap.addTo(map);
  farm = L.layerGroup();
  farm.addTo(map);
  var overlayMaps = {"AMAP": amap, "Paysans": farm};
  L.control.layers({}, overlayMaps).addTo(map);
  $(".leaflet-control-layers").addClass("leaflet-control-layers-expanded");

  // Get data from backend
  $.post(clicamap_map_obj.ajax_url, {
    _ajax_nonce: clicamap_map_obj.nonce,
    action: "clicamap_map_data",
  }, updateCard);

});


function updateCard(data)
{
  data.forEach(function (item) {
    var lat = parseFloat(item.coordGps.latitude);
    var long = parseFloat(item.coordGps.longitude);
    if(isNaN(lat) || isNaN(long) ) {
      return;
    }

    if (item.type === 'ferme') {
      L
        .marker([lat, long], {icon: vmPaysIcon})
        .bindPopup(
          '<span class="paysan-popup popup-title">'
          + item.nom
          + '</span><span class="paysan popup-subtitle">Produits : </span><span class="paysan-popup">'
          + item.typeProduction.join(', ')
          + '</span>'
        )
        .addTo(farm);
    }

    if(item.type === 'amap') {
      var popUp =  '<span class="amap-popup popup-title">'
        + item.nom
        + '</span><br><span class="amap popup-subtitle">Produits : </span>'
        + item.typeProduction.join(', ')
        + '<br><span class="amap popup-subtitle">Livraisons : </span><br>'
        + item.adresse.rue + ' ' + item.adresse.cp + ' ' + item.adresse.ville;

      item.livraisonHoraires.forEach(function (schedule) {
        popUp += '<br>'
          + schedule.saison
          + ': '
          + schedule.jour
          + ' de '
          + schedule.debut
          + ' à '
          + schedule.fin;
      });

      var contacts = '';
      if(item.email !== null && item.email !== '') {
        contacts += '<br>@:<a href="mailto:' + item.email + '">'+ item.email +'</a>'
      }
      if(item.url !== null && item.url !== '') {
        contacts += '<br><a href="' + item.url + '" target="_blank">' + item.url + '</a>'
      }
      if(contacts !== '') {
        popUp += '<br><span class="amap popup-subtitle">Contacts:</span>' + contacts;
      }

      L
        .marker([lat, long], {icon: vmAmapIcon})
        .bindPopup(popUp)
        .addTo(amap);
    }
  });
}
