zip: .clean
	zip -r clicamap-carte.zip assets includes templates clicamap-carte.php index.php LICENSE.txt Makefile

.clean:
	rm clicamap-carte.zip

