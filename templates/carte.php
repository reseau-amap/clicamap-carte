<div
    id="clicamap-map"
    style="height:500px"
    data-init-zoom="<?=isset($options['init_zoom']) ? esc_attr($options['init_zoom']) : 1?>"
    data-init-lat="<?=isset($options['init_pos_lat']) ? esc_attr($options['init_pos_lat']) : 0?>"
    data-init-long="<?=isset($options['init_pos_long']) ? esc_attr($options['init_pos_long']) : 0?>"
></div>
