<?php

require_once 'ClicAmapCommunication.php';

class ClicAmapCacheManager
{
    const CACHE_DURATION_DEFAULT = 60;

    public function getData()
    {
        $data = get_transient( 'clicamap_map_data' );
        if($data === false) {
            $communication = new ClicAmapCommunication();
            $data = $communication->getData();

            $options = get_option( 'clicamap_options' );
            $cacheDuration = isset($options['map_cache']) ? (int)$options['map_cache'] : self::CACHE_DURATION_DEFAULT;
            set_transient('clicamap_map_data', $data, $cacheDuration * 60);
        }
        return $data;
    }
}
