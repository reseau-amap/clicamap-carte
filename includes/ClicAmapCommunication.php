<?php


class ClicAmapCommunication
{
    const URL = 'https://api.clicamap.org';

    /**
     * @var string
     */
    private $authToken;

    /**
     * @var string
     */
    private $filter;

    public function __construct()
    {
        $options = get_option( 'clicamap_options' );
        $this->authToken = 'Basic ' . base64_encode( $options['api_login'] . ':' . $options['api_pass'] );
        $this->filter = isset($options['map_filter']) ? $options['map_filter'] : '';
    }

    public function getData()
    {
        $url = sprintf('%s/api/carte/marqueurs?cp=%s',self::URL, $this->filter);
        $response = wp_remote_get(
            $url,
            [
                'headers' => [
                    'Authorization' => $this->authToken,
                ],
                'user-agent' => '', // Avoid francesrv error,
                'timeout' => 10
            ]
        );
        if ( is_wp_error( $response ) || $response['body'] === '') {
            $error_message = 'Erreur de requete' . $response->get_error_message();
            throw new \RuntimeException($error_message);
        }

        $decoded = json_decode($response['body'], true);
        if(json_last_error() !== JSON_ERROR_NONE) {
            throw new \RuntimeException('Erreur de decodage');
        }

        return $decoded['hydra:member'];
    }
}
