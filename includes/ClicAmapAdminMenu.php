<?php

class ClicAmapAdminMenu
{
    public function addAdminSubmenu()
    {
        add_options_page(
            'Clicamap Carte',
            'ClicAmap',
            'manage_options',
            'clicamap',
            [$this, 'adminSubmenu']
        );
    }

    public function adminSubmenu()
    {
        // check user capabilities
        if (!current_user_can('manage_options')) {
            return;
        }

        include (plugin_dir_path( __FILE__ ) . '../templates/admin.php');
    }

    public function initAdmin()
    {
        register_setting(
            'clicamap',
            'clicamap_options',
            [
                'sanitize_callback' => [$this, 'sanitize']
            ]
        );

        add_settings_section(
            'api',
            'Paramètres d\'API',
            [$this, 'blankHeader'],
            'clicamap'
        );

        add_settings_section(
            'map',
            'Paramètres de la carte',
            [$this, 'blankHeader'],
            'clicamap'
        );

        add_settings_field(
            'clicamap_api_login',
            'Login',
            [$this, 'apiLoginField'],
            'clicamap',
            'api'
        );

        add_settings_field(
            'clicamap_api_pass',
            'Mot de passe',
            [$this, 'apiPassField'],
            'clicamap',
            'api'
        );

        add_settings_field(
            'clicamap_cache_duration',
            'Durée du cache',
            [$this, 'apiCacheField'],
            'clicamap',
            'map'
        );

        add_settings_field(
            'clicamap_filter',
            'Départements à inclure',
            [$this, 'apiFilterField'],
            'clicamap',
            'map'
        );

        add_settings_field(
            'clicamap_init_pos',
            'Position initiale',
            [$this, 'apiInitPosField'],
            'clicamap',
            'map'
        );

        add_settings_field(
            'clicamap_init_zoom',
            'Zoom initial',
            [$this, 'apiInitZoomField'],
            'clicamap',
            'map'
        );

    }

    public function blankHeader()
    {
        return;
    }


    public function apiLoginField($args)
    {
        $options = get_option( 'clicamap_options' );
        $optionEscaped = isset($options['api_login']) ? esc_attr($options['api_login']) : '';
        echo "<input name='clicamap_options[api_login]' type=\"text\" value=\"$optionEscaped\" required>";
    }

    public function apiPassField($args)
    {
        $options = get_option( 'clicamap_options' );
        $optionEscaped = isset($options['api_pass']) ? esc_attr($options['api_pass']) : '';
        echo "<input name='clicamap_options[api_pass]' type=\"password\" value=\"$optionEscaped\" required>";
    }

    public function apiCacheField($args)
    {
        $options = get_option( 'clicamap_options' );
        $optionEscaped = isset($options['map_cache']) ? esc_attr($options['map_cache']) : ClicAmapCacheManager::CACHE_DURATION_DEFAULT;
        echo "<input name='clicamap_options[map_cache]' type=\"text\" value=\"$optionEscaped\" required>";
        echo '<p class="description">Durée du cache en minutes. 0 Pour illimité (non recommandé).</p>';
    }

    public function apiFilterField($args)
    {
        $options = get_option( 'clicamap_options' );
        $optionEscaped = isset($options['map_filter']) ? esc_attr($options['map_filter']) : '';
        echo "<input name='clicamap_options[map_filter]' type=\"text\" value=\"$optionEscaped\">";
        echo '<p class="description">Code des départements à inclure sur la carte, séparés par des virgules.</p>';
    }

    public function apiInitPosField($args)
    {
        $options = get_option( 'clicamap_options' );
        $initPosLatEscaped = isset($options['init_pos_lat']) ? esc_attr($options['init_pos_lat']) : 0;
        $initPosLongEscaped = isset($options['init_pos_long']) ? esc_attr($options['init_pos_long']) : 0;
        echo "<label for='clicamap_options[init_pos_lat]'>Latitude   </label><input name='clicamap_options[init_pos_lat]' type=\"text\" value=\"$initPosLatEscaped\"><br />";
        echo "<label for='clicamap_options[init_pos_long]'>Longitude   </label><input name='clicamap_options[init_pos_long]' type=\"text\" value=\"$initPosLongEscaped\">";
    }

    public function apiInitZoomField($args)
    {
        $options = get_option( 'clicamap_options' );
        $optionEscaped = isset($options['init_zoom']) ? esc_attr($options['init_zoom']) : 1;
        echo "<input name='clicamap_options[init_zoom]' type=\"text\" value=\"$optionEscaped\">";
    }

    public function sanitize($options)
    {
        $cache = (int)$options['map_cache'];
        if($cache < 0) {
            $cache = ClicAmapCacheManager::CACHE_DURATION_DEFAULT;
        }
        $options['map_cache'] = $cache;

        if($options['map_filter'] !== '') {
            $filters = explode(',', $options['map_filter']);
            $filters = array_map(function ($dep) {
                return trim($dep);
            }, $filters);
            $options['map_filter'] = implode(',', $filters);
        }

        return $options;
    }
}
